@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    {{--This sets the heading for the page as add answer--}}
                    <div class="panel-heading">Add Answer</div>

                    <div class="panel-body">

                        {{--this opens a form and links the @store function in the AnswerController which allows a completed answer to be stored to the database--}}
                        {!! Form::open(array('action' => 'AnswerController@store', 'id' => 'createanswer')) !!}

                        {{--this creates a text box that allows the user to input an answer
                        There is a hidden field which takes the question id the answer is linked to and stores it with the answer in the database table so that the 2 are linked--}}
                        <div>
                            {!! Form::label('Answer', 'Answer') !!}
                            {!! Form::text('answer_text', null) !!}
                            {!! Form::hidden('question_id', $question->id) !!}
                        </div>

                        <div>

                            {{--this button submits the answer to the database when it is complete--}}
                            {!! Form::submit('Add Answer', ['class' => 'button']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection