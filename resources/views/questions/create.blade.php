@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Question</div>

                    <div class="panel-body">

                        {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}

                        <div>
                            {!! Form::label('Question', 'Question') !!}
                            {!! Form::text('question_text', null) !!}
                            {!! Form::hidden('questionnaire_id', $questionnaire->id) !!}
                            {!! Form::hidden('author_id', $questionnaire->user->id) !!}
                        </div>

                        <div>

                        {!! Form::submit('Add Question', ['class' => 'button']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection