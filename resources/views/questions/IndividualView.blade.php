@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Question review -  {{ $question->question_text }}</div>

                    <div class="panel-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>Question</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $question->question_text}}</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>Answers Included;</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($question->answer as $answer)
                                <tr>
                                    <td>{{ $answer->answer_text }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                            <a href="http://localhost:8000/answers/create/{{ $question->id }}">Add Answer</a>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
