@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Home Page</div>

                <div class="panel-body">
                    <p>You are logged in!</p>
                    <p>From here you can View all questionnaires or create a brand new questionnaire.</p>
                    <ul>
                        <li><a href="{{ url('/questionnaires') }}">View all Questionnaires</a></li>
                        <li><a href="{{ url('/questionnaires/create') }}">Create a Questionnaire</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
