@extends('layouts.app')
{{--this section contains all of the information that is on the page--}}
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    {{--This sets the heading for the page as create questionnaire--}}
                    <div class="panel-heading">Create Questionnaire</div>
                    {{--This opens a form that allows for a questionnaire to be created, it uses the @store function in the questionnairecontroller--}}
                    <div class="panel-body">


                        {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}

                            {{--This will create a text box that will labelled title and will allow for a questionnaire title to be added
                            There is a hidden field which contains the author-id which is taken from the user who is currently logged into the system--}}
                            <div>
                                {!! Form::label('title', 'Title') !!}
                                {!! Form::text('title', null) !!}
                                {!! Form::hidden('author_id', "$user->id") !!}
                            </div>

                        {{--This will create a text box labelled description which will allow the user to enter a questionnaire description to the questionnaire--}}
                            <div>
                                {!! Form::label('description', 'Description') !!}
                                {!! Form::text('description', null) !!}
                            </div>

                        {{--this div section contains a button which will submit the questionnaire to the database when pressed--}}
                        <div>
                            {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection