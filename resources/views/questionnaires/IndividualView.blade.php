@extends('layouts.app')

{{--this section contains all of the information that is on the page--}}
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    {{--This sets the heading for the page as Questionnaire review then gets the specific questionnaire's title from the database--}}
                    <div class="panel-heading">Questionnaire review -  {{ $questionnaire->title }}</div>

                    {{--this section shows the questionnaire's title by getting the questionnaire title from the database--}}
                    <div class="panel-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>Title</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $questionnaire->title }}</td>
                            </tr>
                            </tbody>
                        </table>

                        {{--this section shows the questionnaire's description by getting the questionnaire description from the database--}}
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>Description</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $questionnaire->description }}</td>
                            </tr>
                            </tbody>
                        </table>

                        {{--this section shows the questionnaire's creator by getting the questionnaire creator from the user table linked ot the questionnaire table from the database--}}
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>Created By</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $questionnaire->user->name }}</td>
                            </tr>
                            </tbody>
                        </table>

                        {{--this section lists all questions that are linked to the questionnaire--}}
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>Questions Included;</td>
                            </tr>
                            </thead>
                            <tbody>

                            {{--This says for each possible questionnaire it returns a single instance and it takes the questions linked to it as a variable--}}
                            @foreach ($questionnaire->question as $question)
                                <tr>
                                    <td>{{ $question->question_text }}</td>
                                    {{--this view answers works by linking to the questions/individual/ question id page that will show an individual question's answers--}}
                                    <td><a href="http://localhost:8000/questions/individual/{{ $question->id }}"> View Answers </a></td>
                                </tr>
                            @endforeach

                            </tbody>
                            {{--this is a link which allows for a question to be added from the questionnaire page--}}
                            <a href="http://localhost:8000/questions/create/{{ $questionnaire->id }}">Add Question</a>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection