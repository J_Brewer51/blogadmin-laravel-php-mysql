@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">


                    <div class="panel-heading">Questionnaires</div>

                    <div class="panel-body">
                        <section>


                            @if (isset ($questionnaires))


                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <td>Title</td>
                                        <td>Description</td>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    @foreach ($questionnaires as $questionnaire)
                                        <tr>
                                            <td>{{ $questionnaire->title }}</td>
                                            <td>{{ $questionnaire->description }}</td>
                                            <td><a href="questionnaires/individual/{{ $questionnaire->id }}"> View Questionnaire </a></td>
                                        </tr>

                                    @endforeach
                                    </tbody>

                                </table>
                            @else
                                <p> No Questionnaires Added</p>
                            @endif


                        </section>
                        <a href="http://localhost:8000/questionnaires/create">Add Questionnaire</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


