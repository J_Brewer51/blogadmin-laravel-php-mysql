angular.module('categoryService', [])

    .factory('Cateory', function($http) {
        return {
            get : function() {
                return $http.get('/api/v1/categories');
            }
        }
    });