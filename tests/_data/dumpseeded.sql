-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: blogadmin
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_category` (
  `article_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `article_category_article_id_index` (`article_id`),
  KEY `article_category_category_id_index` (`category_id`),
  CONSTRAINT `article_category_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `article_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_title_unique` (`title`),
  KEY `articles_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=9010 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (9005,'Est dolores iste ipsa.','Ipsa ut sint sit qui perferendis. Provident dignissimos et officia tenetur. Et aut ut minima rem magnam ducimus fugit.','similique',1,'2017-03-07 14:08:49','2017-03-07 14:08:49','0000-00-00 00:00:00'),(9006,'Voluptatem voluptate dolores laboriosam.','Eaque aut nesciunt repudiandae. Ut qui et perferendis voluptatem. Fugit ut nostrum quam quae officia eos. Tenetur id nesciunt sunt neque quaerat fugit voluptas. Dolorem molestiae debitis eos atque sed.','consequatur',1,'2017-03-07 14:08:49','2017-03-07 14:08:49','0000-00-00 00:00:00'),(9007,'Enim a et ut.','Et iure eius nihil mollitia adipisci. Culpa minus eligendi enim animi enim ipsam dolores. Molestias sed dignissimos libero. Perspiciatis occaecati accusantium aut impedit rem veritatis.','libero',1,'2017-03-07 14:08:49','2017-03-07 14:08:49','0000-00-00 00:00:00'),(9008,'Maxime ut impedit dicta et est qui voluptatum.','Natus sed consequatur earum. Iste voluptatem rem vitae neque nihil eaque repudiandae dignissimos. Aut nostrum et autem. Voluptates blanditiis rem fugiat porro aperiam sed maxime.','nihil',1,'2017-03-07 14:08:49','2017-03-07 14:08:49','0000-00-00 00:00:00'),(9009,'Id dignissimos commodi et repudiandae voluptatem perspiciatis.','Ipsam id dicta reiciendis fugit aut optio. Recusandae rerum nulla molestiae dolorem sed est quia. Placeat rem perspiciatis porro reprehenderit voluptatem.','distinctio',1,'2017-03-07 14:08:49','2017-03-07 14:08:49','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=9902 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'HTML','html',NULL,NULL),(2,'PHP','php',NULL,NULL),(3,'CSS','css',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_19_100050_create_roles_table',1),('2016_02_19_100108_create_permissions_table',1),('2016_02_19_100123_create_articles_table',1),('2016_02_19_100137_create_categories_table',1),('2016_02_19_100200_create_role_user_table',1),('2016_02_19_100219_create_permission_role_table',1),('2016_02_19_100236_create_article_category_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (20,1),(21,1),(22,1),(23,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (20,'see_adminnav','Can see the admin nav',NULL,NULL),(21,'update_role','Can update a users roles',NULL,NULL),(22,'create_role','Can create a new role',NULL,NULL),(23,'see_all_users','Can see users page',NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(2,1),(3,1);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','Admin',NULL,NULL),(2,'Editor','Editor',NULL,NULL),(3,'Author','Author',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10050 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'James','James@example.com','$2y$10$yT/SE9sX6pH9tHoEnzkTl.o6kOQ0.YGhmIC7b5anH9W3YG/XhpgV2','EScSI2ECSa',NULL,NULL),(10000,'Christiana Blanda','vincent70@example.org','$2y$10$uLruZQtwGiCjbSwMxc5S3eKw8Yo1IFli1DMt2LkIOKtSSKlUOMWhG','oXAgINLQDb','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10001,'Heath Bruen III','tillman.norris@example.org','$2y$10$NKAPBsunAtKL58AkNM1l..6SbehfNvdZOlaOlWr.9xPlKXamd7Go6','8Be2d9pitU','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10002,'Mr. Garrick Luettgen MD','vjenkins@example.org','$2y$10$iwa5TSPPeTf3Y4fs1GDs0eM/5szeUrFdm32AjGqjaG2t4gfHMmdJa','aAcH8FFpRa','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10003,'Prof. Silas Mante','leuschke.brayan@example.org','$2y$10$EYIKS3A83bRDffHJ4fW0a.VpSIf7hTe4pcbzRnL2IR5umH0nVjJJe','zOGk7zxCPm','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10004,'Aurelia Kerluke MD','cecelia.sanford@example.com','$2y$10$AN43Km/YxwqgJMX1zCNYf.g2tiwb/HueKGUhMbAkMcoT6oTCFxIP6','TAq4M6t4ZN','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10005,'Mr. Kristian Hane III','dorn@example.net','$2y$10$5YYBfCUqCLddBk/axqBV8eZM1hnEgVPZBclJjdP0uTzRzdcdi52/S','jPUc7ZaKKS','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10006,'Ms. Alberta Jaskolski I','wnicolas@example.com','$2y$10$XHtEwTWt1pEe/UeBOgzUbu29nubgVJtu6KfAbslZXsHP8YhgqokqC','d9JuQMHPxp','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10007,'Judy Schinner','branson02@example.com','$2y$10$9I/D0WBdE1xaqx9Z90G45.t7XbN6yDoAf6hp55lw2q3UwiSY8Simq','rFe6seesmF','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10008,'Prof. Jayce Wisoky','era.koelpin@example.org','$2y$10$nocd.H14Z/udUe7JjWeyUe60Mpu/anZ9i.sAhVtlYd8JqmQkDGsee','QdE1pSpMFm','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10009,'Izabella Gutmann','ljacobs@example.org','$2y$10$95PtSBCYC64iaJCY72hgCecyI4QcJSu0KZdHEFGHTSWKllJw2G9.2','3IyQ5NPRnJ','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10010,'Naomie Donnelly','mankunding@example.org','$2y$10$jhm77IrYte.g/pMFhYOY1OPQfCa0olZ1F5TQSgduLmDMUU9q5ko7y','4M47QYF0SY','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10011,'Rodrigo Smith','schaden.ima@example.org','$2y$10$zyxnG8kvNeNRNIwYq6IZb.YIVOlBNNk/kkJkpt3jGWsYocMZLfr.G','dnHmZePYDu','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10012,'Archibald Little','kristian05@example.net','$2y$10$xWAh4hh/RaAC3chS/w1nEen3OmHCSAEyv9QgE0qcSgC2SiSqY.Fjq','YPd7vDVs2s','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10013,'Brielle Johnston','hhodkiewicz@example.net','$2y$10$WGF0SstYf9dySiXw8ssUIOnIL2iq6Q..nfBupJGkHmVD6xp8Ib1Wa','MQ9VyHTd2K','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10014,'Connor Lakin','dhudson@example.com','$2y$10$IM.NAuyX/GgU4Zu0/Xrfcua8hnewi7T1PDsbgKNL6QL6w2BzoA3rG','JENb1KXxTp','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10015,'Araceli Dibbert','willy.goodwin@example.net','$2y$10$g5Drzm6Ou6fYzSD6uRJoOuRMYEU2GtzLoL6A/ez3.iV2iAFMNhHMO','GwgvdusoVd','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10016,'Margaretta Nienow','wwilderman@example.net','$2y$10$yMHcUxzGlKEVsIA4AhnMbe.zo7yb.DL6GnrWfPEF5b6asx6gAsHAi','kRHShbqygU','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10017,'Prof. Casimir Torp I','bogisich.judy@example.net','$2y$10$NJfsWIpdSdvZT8k9lDmYheenQ3/HCmD0z8JWBTN0A7W3YCudtozXW','V8LlWwHKi0','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10018,'Edmund Barton','zion.cronin@example.com','$2y$10$jwuVxxz980mS8H7i3LI3GequR4WPwjIUjXGVeTH/4AZvemrvwKuIO','CHXWrPorsC','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10019,'Valerie Jaskolski','nschinner@example.org','$2y$10$BsEaXoZtvWaWun1MCWglMOl2Q8aIhvGLsLMA8G.2AVPa5eyO.Pv12','5bjPgDEXtU','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10020,'Laurianne Abbott','pthiel@example.org','$2y$10$qC9sc.RIOBg/PmzvvSuWKelUH7ROqTBKA8JD2u1PSIsa26b.4Wn6C','dL0nW4TgdM','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10021,'Lyla Bergstrom','emoen@example.org','$2y$10$IhJLiizvA7Tf0yD88uTsnOawWxvWMVoIfaPR4cyfSZcEeI3/9ZEeu','i1EabgFg9G','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10022,'Kraig Friesen','tthompson@example.com','$2y$10$3KmaVJAVWKOwqybbc9o85.ctQZnRqllQbN4L6LOfeOufeWDtBwR7u','SRXE4FdEcO','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10023,'Ms. Orpha Jerde','baumbach.myrl@example.com','$2y$10$l.arAbuxX2Dov2xpDE87CO7amk8r6H6dt7wPk9OkzHFREQVCiP49q','ZVGZVAsAHn','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10024,'Mr. Talon Kemmer V','lydia21@example.net','$2y$10$Zyvkb5TuHg0Qzh0IQQk11.dEqYEFk6Lw2BfHeD/3cUy1uvpyTJuTK','oLrp9Jn0H2','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10025,'Mrs. Ozella Maggio II','kessler.emanuel@example.com','$2y$10$aM6m7sjpdhuDenHT4Ev9e.iR4O8MXapzc9oPHKjF7zA0yqe.fAPHa','PmJ0oF9PFG','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10026,'Arno Littel','omcclure@example.org','$2y$10$sUPQRqLCCHdHNtPAqBxnv.jbTONhe03TqsJcD3TJdah79FL/z86I6','vMymOelvA3','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10027,'Miss Kailey Herman Sr.','edna.wunsch@example.com','$2y$10$lvVqM0dCJWYC.A8jYygjYuGCY8eLWisDepqIfbOFUSfBDYsVtLqlG','DowI8SUfFy','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10028,'Ricky Crooks','kovacek.johnpaul@example.net','$2y$10$2z03T6kIncu2c4oIl.kd5.vnIjC9qHx/FueaJhM1ahRkv2XyPbzSu','BkeHkN8t3E','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10029,'Prof. Gavin Metz Jr.','fatima56@example.org','$2y$10$aONeVU6T2M7lxZxUfV08..gOhBlQNrevw53MHo7q1YjoRlcRoXeQ2','jfmsyhTCa8','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10030,'Deontae Rau DDS','naomi.rosenbaum@example.net','$2y$10$TqKs2neefnEVaqsgpR71.OqtDWUBnGyQkE5daKgyRLxIID7ui6WOG','MHzZFZKOSH','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10031,'Miss Shakira Gutkowski','jast.vilma@example.com','$2y$10$Nih6tuB9K8dqD2cnvzr4mu3mtgy1VuhnY620Uc6TQteNqMxYlIMaS','z81BJ98x6x','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10032,'Prof. Amya Lind','walsh.althea@example.com','$2y$10$yTnk7qq/OQY/FUWPCiZ4J.8fSzJ/5tWL7/TIrKaiNt0CMcnYnE5Xq','iwu7w4I2K3','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10033,'Dr. Camden Koch PhD','collier.tre@example.org','$2y$10$46H0UYXMW6nQWszzC2BYmOEHbeVQ1IaT4pCHazsveDSo2JJgCdMPi','jV0Bwb9SqF','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10034,'Annabelle Gottlieb','justice74@example.com','$2y$10$XXhqiw.lpMbxtH8T2m2FOeVo6Bz/dkNp0ukUae/2McWwZX6TSA.Ou','PHlvOdBGLL','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10035,'Mr. Bailey D\'Amore Sr.','renner.lyla@example.org','$2y$10$Sb8iEO1xMqbMIv949.bB6O9d.qm9Nsbqy4xZ61YRbBWDKDEDGC76u','y6vOaLJ5bC','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10036,'Marilie Collier','geraldine.kuvalis@example.com','$2y$10$tARZ7I4BodSQYzJRZ1i.helxJ/mvz4OQCCVHdNjGJ9YX523hsXFdS','BmZQOamcxI','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10037,'Prof. Theo Wunsch','sophia.dare@example.net','$2y$10$.gAJ9ziS5hQsQxGUGfuc7OAvrzN9G7wKtmcP.ubUYRKnJ/z8TCPia','Q8KGDXHMcj','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10038,'Prof. Axel Dickinson DDS','pkemmer@example.net','$2y$10$oISsqj21QPiQWe8E9kmrK.5.mBkz14o5q/VgRhv3QQrYBy.VyQdzi','3jfTbKgNRc','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10039,'Ms. Carolyn Ziemann PhD','considine.davin@example.com','$2y$10$ITt/QQO7GCKRQPWxEySLgeOziwBN2Yo22gBDOS9Wwf8NPyPFEfjdK','fvSVXiCn2P','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10040,'Mrs. Vergie Schultz','morar.adell@example.net','$2y$10$3a3xNewjtJqt8dsCEZqA7.DfKMc1VI/hg.bb4esendz/DssKT61G2','ZApivTWZbW','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10041,'Emie Aufderhar','norbert77@example.org','$2y$10$t0zeKVJEEuy61PHpS1mHhOJnEMnWmSItGhjIuLAUI/UeVWDwqTDye','O2DP01jpD3','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10042,'Rigoberto Champlin','omurphy@example.net','$2y$10$z/hnN5pEfLWwIgQkd08VGe4Npie0iDmnO8zzxFmAc7Lj71hE663Sq','po1Nz3T3q0','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10043,'Kennith Howell','abagail.streich@example.com','$2y$10$gGhO1ObJCdv609C/wGWnjOQZgqTQTP6mkbfhgpL5nxerQQ/BiOFb.','lxViFm9O3T','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10044,'Tillman Cronin','verla11@example.org','$2y$10$DlfxovVCrxkTehc.1ugTTeaGNSW.udfq712Nh7xWkF/wjoyvKd/Yi','JFkdG56pYo','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10045,'Alvis Friesen','gino33@example.org','$2y$10$WlbJ/T0QOV1hvHMFBBNj1.3TkW8pzj2RjY7LpJIgpEWwXtuOkq7Ye','2Tk52x0Bly','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10046,'Miss Abbey Pfannerstill','candace97@example.com','$2y$10$wnhTTwgXK/2j3V1uGaJ.u.VG6/FBqaHV5hIv7eI3W6gzVLq1CF3SW','lXRorfZijh','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10047,'Nikki Goldner','jedidiah.hagenes@example.org','$2y$10$wIJ31qgdd8Y/SquMl.UzjOeUV9P8B2.d.WvrA.eiU9ugvBdSGI/di','SeSCf3v501','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10048,'Nicklaus Parisian','mleffler@example.com','$2y$10$4L8KeR0Xk8fRBx4O7izrLuQodowuSX3lKzLQe6OFxHxMMl.Ahtb46','1n1RxK6KHe','2017-03-07 14:08:49','2017-03-07 14:08:49'),(10049,'Leo Morar','umorar@example.net','$2y$10$h4vMqrBtBEEpUgdDTMOTz.xXcPPzOI4cI/SLkylSmKlxJEkH8rBcK','WmX4yILNVv','2017-03-07 14:08:49','2017-03-07 14:08:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-07 14:17:21
