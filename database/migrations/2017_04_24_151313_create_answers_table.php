<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * This shows what data fields will be added into the new database table
     * the data type is shown along with the field name after it
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer_text');
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions');
            $table->timestamps();
        });
    }

    /**
     * This function drops the database table called answers if the migration is re-run
     * this will remake the table with any changes made above
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
