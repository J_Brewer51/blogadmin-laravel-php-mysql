<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnaireTable extends Migration
{
    /**
     * This shows what data fields will be added into the new database table
     * the data type is shown along with the field name after it
     */
    public function up()
    {
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->integer('author_id')->unsigned()->default(0);
            $table->foreign('author_id')->references('id')->on('users');
            $table->timestamps();
            $table->timestamp('published_at')->index();
        });
    }

    /**
     * This function drops the database table called questionnaires if the migration is re-run
     * this will remake the table with any changes made above
     */
    public function down()
    {
        Schema::drop('questionnaires');
    }
}
