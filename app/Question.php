<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * This is an array and shows what data can queried from the database
     */
    protected $fillable = [
        'question_text',
        'questionnaire_id',
        'author_id',
    ];

    /**
     * this function returns a relationship from the user model
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * returns a value that belongs to app\questionnaire
     */
    public function questionnaire()
    {
        return $this->belongsTo('App\Questionnaire');
    }

    /**
     * This function shows how a questions can have many answers
     */
    public function answer()
    {
        return $this->hasMany('App\Answer', 'question_id');
    }
}