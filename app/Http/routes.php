<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
    Route::resource('/admin/users', 'UserController' );
    /**
     *  These routes are for the various questionnaire features,
     *  viewing all questionnaires, viewing an individual questionnaire and creating questionnaires
     *  They relate to the questionnaire controller page and the '@' refers to a specific function
     */
    Route::resource('questionnaires', 'QuestionnaireController');
    Route::resource('questionnaires/create', 'QuestionnaireController@create');
    Route::resource('questionnaires/individual', 'QuestionnaireController@individual');
    /**
     *  These routes are for the various question features,
     *  viewing an individual question and creating questions
     *  They relate to the question controller page and the '@' refers to a specific function within the controller
     */
    Route::resource('questions', 'QuestionController');
    Route::resource('questions/create', 'QuestionController@create');
    Route::resource('questions/individual', 'QuestionController@individual');
    /**
     *  These routes are for the ability to create an answer for a question
     *  The route links to the answers controller and the @create function which allows for answers to be created
     */
    Route::resource('answers', 'AnswerController');
    Route::resource('answers/create', 'AnswerController@create');
});