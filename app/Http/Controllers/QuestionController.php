<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * This is telling the controller to use the different models to get data from the database, and controllers that have been created
 * It is using the auth files to enable user information
 * The questionnaire and question models are being used
 * This allows for the relations between objects to be set
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Question;
use App\Questionnaire;


class QuestionController extends Controller
{
    /**
     * This is the function to create a new question within a questionnaire
     * it sets the variable $questionnaire as the value of the Questionnaire id
     * it then returns the view of question.create which is a blade file with questionnaire as the $questionnaire variable
     */
    public function create($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);

        return view('questions.create', ['questionnaire' => $questionnaire]);
    }

    /**
     * This is the function to view an individual question and the answers within the question
     */
    public function individual($id)
    {
        $question = Question::where('id',$id)->first();

        return view('questions.IndividualView', ['question' => $question]);
    }

    /**
     * This is the function that will store a created question
     */
    public function store(Request $request)
    {
        $question = Question::create($request->all());
        $question->save();

        return view('questions.IndividualView', ['question' => $question]);
    }

}