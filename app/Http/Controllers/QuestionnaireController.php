<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * This is telling the controller to use the different models to get data from the database and controllers that have been created
 * It is using the auth files to enable user information
 * The questionnaire model is being used
 * This allows for the relations between objects to be set
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Questionnaire;

class QuestionnaireController extends Controller
{
    /**
     *  This function allows for all questionnaires to be displayed
     */
    public function index()
    {
        $questionnaires = Questionnaire::all();

        return view('questionnaires.ShowQuestionnaires', ['questionnaires' => $questionnaires]);
    }
    /**
     *  This function allows for questionnaires to be created
     * it will return the questionnaires.create view afterwards
     */
    public function create()
    {
        $user = Auth::user();
        return view('questionnaires.create', ['user' => $user]);
    }
    /**
     *  This function allows for an individual questionnaire to be displayed
     */
    public function individual($id)
    {
        $questionnaire = Questionnaire::where('id',$id)->first();

        return view('questionnaires.IndividualView', ['questionnaire' => $questionnaire]);
    }

    /**
     *  This function allows for a created questionnaire to be stored in the database
     */
    public function store(Request $request)
    {
        $questionnaire=Questionnaire::create($request->all());
        $questionnaire->save();
        return redirect('questionnaires');
    }
}
