<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * This is telling the controller to use the different models to get data from the database that have been created
 * It is using the auth files to enable user information
 * The answer and question models are being used
 * This allows for the relations between objects to be set
 */
use Auth;
use App\Answer;
use App\Question;

class AnswerController extends Controller
{
    /**
     * This is the function to create a new answer for a question
     * sets the variable $question as the Question and gets the question id
     * it then returns the view of answers.create which is a url, where the question is equal to the $question variable that was set above
     * This then allows for an answer to be created within a question
     */
    public function create($id)
    {
        $question = Question::findOrFail($id);
        return view('answers.create', ['question' => $question]);
    }

    /**
     * This function allows for a created answer to be stored in the mysql database
     * it sets the variable $answer as Answer then creates a request to the database which stores the data
     * It the saves the $answer and returns the view
     */
    public function store(Request $request)
    {
        $answer = Answer::create($request->all());
        $answer->save();

        return redirect('questionnaires');
    }
}
