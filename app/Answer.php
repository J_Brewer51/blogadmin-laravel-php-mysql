<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * This is an array and shows what data can queried from the database
     */
    protected $fillable = [
        'answer_text',
        'question_id',
    ];

    /**
     * this function shows how an answer is related to a question
     * it is saying that $this, an answer belongs to a question within the App|Question file with an id
     */
    public function questions()
    {
        return $this->belongsTo('App\Question', 'id');
    }

}