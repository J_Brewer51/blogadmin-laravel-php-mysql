<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    /**
     * This is an array and shows what data can queried from the database
     */
    protected $fillable = [
        'title',
        'description',
        'author_id',
    ];

    /**
     * This function returns a value from the app\user model with the author_id field
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    /**
     * this function shows how a questionnaire can have many questions
     */
    public function question()
    {
        return $this ->hasMany('App\Question', 'questionnaire_id');
    }
}